# README #

Source ini adalah sebuah PROTOTYPE framework / template aplikasi yang sedang dalam tahap pengembangan, untuk aplikasi client-server Android  yang menggunakan HttpUrlConnection.

### Apa itu Intent Service? Apa bedanya dengan AsyncTask? ###

* [https://medium.com/android-news/using-intentservice-vs-asynctask-in-android-2fec1b853ff4](https://medium.com/android-news/using-intentservice-vs-asynctask-in-android-2fec1b853ff4)

* [http://stackoverflow.com/questions/15167152/should-i-use-asynctask-or-intentservice-for-my-application](http://stackoverflow.com/questions/15167152/should-i-use-asynctask-or-intentservice-for-my-application)


### Konsep ###
* Semua request (dari class manapun) dihandle secara terpusat pada class PSService, dan hasil dari request tersebut bisa didapat dari kelas manapun, sesuai IntentFilter-nya.
* Saat melakukan request, gunakan IntentFilter untuk memberitahu kemana hasil request tersebut akan dikirim.
* Hasil request (response) akan di broadcast / disebarkan / dikirim kepada kelas yang memiliki filter yang sama saat melakukan request.

### Cara Penggunaan ###

* Extend class ke PSServiceActivity / PSServiceFragment (sesuai kebutuhan)
* Buat Object BroadcastReceiver pada class yang dibuat
* register dan unregister receiver pada method onResume dan onPause
* Untuk mengambil / parsing response, cukup menambahkan method "successResponse" untuk response dengan code 200, dan "badResponse" untuk code selain 200.
* Method tersebut ada di superclass (PSSeviceActivity / PSServiceFragment)
* Untuk lebih detailnya, bisa dilihat pada class TestActivity. 
* Pada contoh, request dan penerimaan response berada pada 1 class yang sama.