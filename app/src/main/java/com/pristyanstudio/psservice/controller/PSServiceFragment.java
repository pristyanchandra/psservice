package com.pristyanstudio.psservice.controller;

import android.support.v4.app.Fragment;

import com.pristyanstudio.psservice.data.ServiceRequest;
import com.pristyanstudio.psservice.service.PSService;

/**
 * Created by User on 11/4/2015.
 */
public class PSServiceFragment extends Fragment {
    ServiceRequest serviceRequest;

    public void sendGetRequest(String uri, String filter){
        serviceRequest.setUri(uri);
        serviceRequest.setMethod(PSService.METHOD_GET);
        serviceRequest.setFilter(filter);
    }

    public void sendPostRequest(String uri, String parameter, String filter){
        serviceRequest.setUri(uri);
        serviceRequest.setMethod(PSService.METHOD_POST);
        serviceRequest.setContent(parameter);
        serviceRequest.setFilter(filter);
    }

    public void sendDeleteRequest(String uri, String parameter, String filter){
        serviceRequest.setUri(uri);
        serviceRequest.setMethod(PSService.METHOD_DELETE);
        serviceRequest.setContent(parameter);
        serviceRequest.setFilter(filter);
    }

    public void sendPutRequest(String uri, String parameter, String filter){
        serviceRequest.setUri(uri);
        serviceRequest.setMethod(PSService.METHOD_PUT);
        serviceRequest.setContent(parameter);
        serviceRequest.setFilter(filter);
    }
}
