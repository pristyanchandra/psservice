package com.pristyanstudio.psservice.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.pristyanstudio.psservice.data.ServiceRequest;
import com.pristyanstudio.psservice.data.ServiceResponse;
import com.pristyanstudio.psservice.service.PSService;

/**
 * Created by User on 11/4/2015.
 */
public class PSServiceActivity extends AppCompatActivity {
    ServiceRequest serviceRequest;

    public void sendGetRequest(String uri, String filter){
        serviceRequest.setUri(uri);
        serviceRequest.setMethod(PSService.METHOD_GET);
        serviceRequest.setFilter(filter);
    }

    public void sendPostRequest(String uri, String parameter, String filter){
        serviceRequest.setUri(uri);
        serviceRequest.setMethod(PSService.METHOD_POST);
        serviceRequest.setContent(parameter);
        serviceRequest.setFilter(filter);
    }

    public void sendDeleteRequest(String uri, String parameter, String filter){
        serviceRequest.setUri(uri);
        serviceRequest.setMethod(PSService.METHOD_DELETE);
        serviceRequest.setContent(parameter);
        serviceRequest.setFilter(filter);
    }

    public void sendPutRequest(String uri, String parameter, String filter){
        serviceRequest.setUri(uri);
        serviceRequest.setMethod(PSService.METHOD_PUT);
        serviceRequest.setContent(parameter);
        serviceRequest.setFilter(filter);
    }
    public BroadcastReceiver getReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ServiceResponse serviceResponse = (ServiceResponse) intent.getSerializableExtra(PSService.TAG_RESPONSE);

                switch (serviceResponse.getCode()) {
                    case 200:
                    case 204:
                        successResponse(serviceResponse, intent.getAction());
                        break;
                    case 400:
                    case 401:
                    case 404:
                        badResponse(serviceResponse, intent.getAction());
                        break;
                    default:
                        badResponse(serviceResponse, intent.getAction());
                        break;
                }
            }
        };
    }


    public void successResponse(ServiceResponse response, String filter){
    }

    public void badResponse(ServiceResponse response, String filter) {

    }
}
