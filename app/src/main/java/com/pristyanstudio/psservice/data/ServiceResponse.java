package com.pristyanstudio.psservice.data;

import java.io.Serializable;

/**
 * Created by Pristyan on 11/1/2015.
 */
public class ServiceResponse implements Serializable {
    private int code;
    private String message;
    private String uri;
    private String method;
    private String content;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}