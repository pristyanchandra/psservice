package com.pristyanstudio.psservice.data;
import java.io.Serializable;

/**
 * Created by Pristyan on 11/2/2015.
 */
public class ServiceRequest implements Serializable {

    private String uri;
    private String method;
    private String filter;
    private String content;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
