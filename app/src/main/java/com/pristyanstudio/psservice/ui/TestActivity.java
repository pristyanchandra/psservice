package com.pristyanstudio.psservice.ui;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.Bundle;

import com.pristyanstudio.psservice.R;
import com.pristyanstudio.psservice.controller.PSServiceActivity;
import com.pristyanstudio.psservice.data.ServiceResponse;

/**
 * Created by User on 11/4/2015.
 */
public class TestActivity extends PSServiceActivity {
    BroadcastReceiver receiver = getReceiver();
    private final String FILTER_TEST = "valuenya.bebas.yang.penting.unik.dan.beda.satu.sama.lain";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*sendPostRequest("uri tujuan", "Parameter yang sudah di-parsing",
                "filter yang akan menerima response dari request ini");*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver,new IntentFilter(FILTER_TEST));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    public void successResponse(ServiceResponse response, String filter) {
        super.successResponse(response, filter);
        /*
        * Di sini dilakukan parsing atau pengolahan response dengan code ~200
        * */
    }

    @Override
    public void badResponse(ServiceResponse response, String filter) {
        super.badResponse(response, filter);
        /*
        * Di sini dilakukan parsing atau pengolahan response dengan code selain ~200
        * */
    }
}
