package com.pristyanstudio.psservice.service;
import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.pristyanstudio.psservice.data.ServiceRequest;
import com.pristyanstudio.psservice.data.ServiceResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.*;

/**
 * Created by Pristyan on 11/1/2015.
 * Pristyan Studio Service (PSService)
 */
public class PSService extends IntentService {

    public static final String TAG_RESPONSE = "serviceResponse";
    public static final String TAG_REQUEST = "serviceRequest";

    public static final String METHOD_GET = "GET";
    public static final String METHOD_POST = "POST";
    public static final String METHOD_PUT = "PUT";
    public static final String METHOD_DELETE = "DELETE";

    String uri;
    String method;
    String filter;
    String parameter;

    int response_code;
    String response_message;
    String response_content;

    public PSService() {
        super("PSService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        URLConnection urlConnection = new URLConnection();

        ServiceRequest serviceRequest = (ServiceRequest) intent.getSerializableExtra(TAG_REQUEST);
        uri = serviceRequest.getUri();
        method = serviceRequest.getMethod();
        filter = serviceRequest.getFilter();
        switch (method) {
            case METHOD_GET:
                urlConnection.Get(uri);
                break;
            case METHOD_POST:
                parameter = serviceRequest.getContent();
                urlConnection.Post(uri, parameter);
                break;
            case METHOD_PUT:
                parameter = serviceRequest.getContent();
                urlConnection.Put(uri, parameter);
                break;
            case METHOD_DELETE:
                parameter = serviceRequest.getContent();
                urlConnection.Delete(uri,parameter);
                break;
        }

        Log.d("PSService", "------------------------------------------------------");
        Log.d("URI", uri);
        Log.d("Filter", filter);
        Log.d("Method", method);
        Log.d("Parameter", parameter);
        Log.d("", "----------------------------------------------------------------");

        ServiceResponse serviceResponse = new ServiceResponse();
        serviceResponse.setUri(uri);
        serviceResponse.setMethod(method);
        serviceResponse.setCode(response_code);
        serviceResponse.setMessage(response_message);
        serviceResponse.setContent(response_content);
        serviceRequest.setFilter(filter);

        Log.d("Service Response", "--------------------------------------------------");
        Log.d("Code", "" + response_code);
        Log.d("Message", response_message);
        Log.d("Method", method);
        Log.d("Content", response_content);

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(filter);
        broadcastIntent.putExtra(TAG_RESPONSE, serviceResponse);
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.sendBroadcast(broadcastIntent);
    }

    public class URLConnection {

        public void Get(String uri) {
            BufferedReader reader = null;
            try {
                java.net.URL url = new java.net.URL(uri);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setReadTimeout(10000);
                con.setConnectTimeout(15000);
                con.setDoInput(true);
                con.connect();
                InputStream is = con.getInputStream();
                reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                String data = null;
                response_code = con.getResponseCode();
                response_message = con.getResponseMessage();
                response_content = "";
                while ((data = reader.readLine()) != null) {
                    response_content += data + "\n";
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        public void Post(String uri, String param){
            try {
                java.net.URL url = new java.net.URL(uri);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setChunkedStreamingMode(0);
                con.setRequestMethod("POST");
                con.getOutputStream().write(param.getBytes());

                response_code = con.getResponseCode();
                response_message = con.getResponseMessage();
                if (response_code == 200) {

                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    response_content = sb.toString();
                } else {
                    response_content = "";
                }

                con.disconnect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void Put(String uri, String param){
            try {
                java.net.URL url = new java.net.URL(uri);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setChunkedStreamingMode(0);
                con.setRequestMethod("PUT");
                con.getOutputStream().write(param.getBytes());

                response_code = con.getResponseCode();
                response_message = con.getResponseMessage();
                if (response_code == 200) {

                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line+"\n");
                    }
                    br.close();
                    response_content = sb.toString();
                }
                con.disconnect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void Delete(String uri, String id){
            try {
                java.net.URL url = new java.net.URL(uri+id);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setChunkedStreamingMode(0);
                con.setRequestMethod("DELETE");
                con.connect();

                response_code = con.getResponseCode();
                response_message = con.getResponseMessage();
                if (response_code == 200) {

                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line+"\n");
                    }
                    br.close();
                    response_content = sb.toString();
                }

                con.disconnect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}